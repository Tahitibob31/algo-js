<------------------------------------------------------------------------ [EXERCICE 1] -------------------------------------------------------------------------->
Algorithme
{Ecrire un algorithme qui demande à l’utilisateur un nombre compris entre 1 et 3 jusqu’à ce que la réponse convienne.}

variable n: entier
n <- 0

début
	Tant que n > 1 ||n < 3 faire
		afficher ("Entrer un nombre")
		saisir n
	ftq
Fin

		
<------------------------------------------------------------------------ [EXERCICE 1] -------------------------------------------------------------------------->
Algorithme
{Ecrire un algorithme qui demande un nombre compris entre 10 et 20, jusqu’à ce que la réponse convienne. En cas de réponse supérieure à 20, on fera apparaître un message : « Plus petit ! », et inversement, « Plus grand ! » si le nombre est inférieur à 10.}

variable n: entier
n <- 0

début
	Tant que n < 10 ||n > 20 faire
		afficher ("Rentrer un nombre")
		Saisir n
		Si n > 20			
			afficher ("Rentrer un nombre plus petit")
			Saisir n
		fsi
		Si n < 10
			Afficher ("Rentrer un nombre plus grand")
			Saisir n
		fsi
	ftq
	afficher ("Bravo")
fin

<------------------------------------------------------------------------ [EXERCICE 3] -------------------------------------------------------------------------->

Algorithme
{Ecrire un algorithme qui demande un nombre de départ, et qui ensuite affiche les dix nombres suivants. Par exemple, si l'utilisateur entre le nombre 17, le programme affichera les nombres de 18 à 27.}

variable n, i: entiers

début
	afficher ("Entrer un nombre)
	Saisir n
	pour i <- n à n+10 [par 1] faire
		Afficher i
	ftq
fin
				
<------------------------------------------------------------------------ [EXERCICE 4] -------------------------------------------------------------------------->

Algorithme
{Réécrire l'algorithme précédent, en utilisant cette fois l'instruction Pour (Si ce n'est pas déjà le cas)}

// VOIR EXERCICE 3


<------------------------------------------------------------------------ [EXERCICE 5] -------------------------------------------------------------------------->
Algorithme
{Ecrire un algorithme qui demande un nombre de départ, et qui ensuite écrit la table de multiplication de ce nombre, présentée comme suit (cas où l'utilisateur entre le nombre 7) : Table de 7 : 7 x 1 = 7 7 x 2 = 14 7 x 3 = 21 ... 7 x 10 = 70}

variable resultat: entier
variable n: entier
variable i: entier


début
	saisir n
	pour i <- 1 à 10 [par 1] faire
		resultat <- i*n
		afficher (,n ,"x" ,i, "=", resultat)
	fpour
fin

<------------------------------------------------------------------------ [EXERCICE 6] -------------------------------------------------------------------------->

Algorithme
{Ecrivez un algorithme calculant la somme des valeurs d’un tableau (on suppose que le tableau a été préalablement saisi).}


variable array, somme,i: entier
array <- [56,75,9,12,94,27,67,42]

début
	pour i <- 0 à fin de array [par 1] faire
		somme <- somme + array[i]
	fpour
	afficher somme
fin

<------------------------------------------------------------------------ [EXERCICE 7] -------------------------------------------------------------------------->

Algorithme
{Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur, et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres : Entrez le nombre numéro 1 : 12 Entrez le nombre numéro 2 : 14 etc. Entrez le nombre numéro 20 : 6 Le plus grand de ces nombres est : 14 Modifiez ensuite l’algorithme pour que le programme affiche de surcroît en quelle position avait été saisie ce nombre : C’était le nombre numéro 2}

variable array: entier
	 i: entier
	
début
	pour i <- 0 à 20 [par 1] faire
		afficher (entrer un nombre)
		saisir <- array[i]
	fpour
	afficher("le nombre le grand est" Math.max(...array) "et il a été le "array.indexOf(Math.max(...array)) "eme nombre tapé")
fin

<------------------------------------------------------------------------ [EXERCICE 8] -------------------------------------------------------------------------->

Algorithme
{Réécrire l’algorithme précédent, mais cette fois-ci on ne connaît pas d’avance combien l’utilisateur souhaite saisir de nombres. La saisie des nombres s’arrête lorsque l’utilisateur entre un zéro.}

variable array, dNumber, vMax, iVMax: entier
c: booléen
c <- false

début
	tant que c !== true faire
		saisir dNumber
		array <- dNumber
		si dNumber === 0
			alors c <- true
		fsi
	ftq
	vMax <- Math.max(...array)
	iVMax <- array.indexOf(Math.max(...array))
	afficher(`le nombre le plus grand est ${vMax} et il a été le  ${iVMax + 1} ème nombre tapé`)
fin
	
<------------------------------------------------------------------------ [EXERCICE 9] -------------------------------------------------------------------------->

Algorithme
[Lire la suite des prix (en euros entiers et terminée par zéro) des achats d’un client. Calculer la somme qu’il doit, lire la somme qu’il paye, et simuler la remise de la monnaie en affichant les textes "10 Euros", "5 Euros" et "1 Euro" autant de fois qu’il y a de coupures de chaque sorte à rendre.]

variable achats, pr, pr2, remise, sommeAll, remiseMod, billetDix, billetCinq, billetUn: entier
	 isZero, isZero2: booléen
constante reducer
remiseMod <- remise
remise <- pr2 - achats.reduce(reducer)
array <- []
isZero,isZero2 <- False
pr2 <- 0
reducer <- (previousValue, currentValue) => previousValue + currentValue
sommeAll <- achats.reduce(reducer)

début
	tant que isZero !== true faire
		afficher('Veuillez rentrer les prix des articles un par un:')
		saisir pr
		achats <- pr
		si pr===0
			alors isZero <- true
		fsi
	ftq
	tant que isZero !== true faire
		afficher('Veuillez rentrer le prix que vous souhaitez mettre pour l'achat de ces articles:')
		saisir pr2
		if achats.reduce(reducer < pr2)
			alors isZero = true
		fsi
	remise <- pr2 - achats
	ftq
	tant que remiseMod >= 10nfaire
		remiseMod <- remiseMod -10
		billetDix [par 1]
	ftq
	tant que remiseMod >= 5 && remised < 10 faire
		remiseMod <- remiseMod -5
		billetCinq [par 1]
	ftq
	tant que remiseMod >= 1 && remiseMod <5 faire
		remisedMod <- remiseMod -1
		billetUn [par 1]
	ftq
	afficher(`Le total à payer est de ${sommeAll}, Vous avez choisi de donner ${remise} et vous recevrez ${billetDix} billet de dix, ${billetCinq} billet de cinq, ${billetUn} de un`)

fin

	
		















